@extends('layout.master')
    @section('judul1')
        Halaman Detail Cast {{$cast->id}}
    @endsection 
    @section('content')
        <h2>Show Post {{$cast->id}}</h2>
        <h4>Nama Pemeran : {{$cast->nama}}</h4>
        <p>Umur Pemeran : {{$cast->umur}}</p>
        <p>Biodata Pemeran : {{$cast->bio}}</p>
    @endsection 