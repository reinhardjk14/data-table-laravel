@extends('layout.master')
    @section('judul1')
        Edit Cast ID {{$cast->id}}
    @endsection

    @section('content')
    <h2>Tambah Data</h2>
    <form action="/cast/{cast->id}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
            @error('namaPemeran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="biodata">Biodata</label>
            <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="biodata" placeholder="Masukkan Biodata">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">EDIT</button>
    </form>
    @endsection